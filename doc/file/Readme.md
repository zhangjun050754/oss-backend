# 对象数据管理

## 数据清理程序（未开发）
- 所有的删除对象操作，均只删除引用，以及减少引用计数，真实的数据删除，由 “定时清理程序”（未开发...）完成
> 这里可以考虑设计一个回收站功能，删除后，将引用关系 移动到 回收站表，N天后自动删除,精确到天的23:59:59,N 天之前可以选择恢复 （N = 7 day）

##  数据分布存储（已实现，单机和分布式模式）

- 如何将数据分片分别存储到不同服务器？

答： http 指定ip 分发?

- 考虑机器增加，减少情况下，系统仍具有稳定性，可用性

答：现有模式下，应指定某一区域（region）对应固定的可用机器IP（至少需要6台）,目前 RS(4+2) 能接受任意2台服务器宕机，保证数据完成可用，也就是说，在减少2台服务器情况下，系统仍然稳定，可用；
服务器增加，需要维护更新 region（region 管理模块未开发...） 对应可用机器列表，实际使用仍只需要 region 中6台即可，这里保证服务容量可横向拓展，当然在数据分发时，需要注意机器权重，以及考虑后续可能的数据迁移（跨区）

- 考虑网络异常下，数据分片的丢失处理，每一个分片存储都要收到确切的成功应答

答： http 分发可以接受并处理响应

## 大文件上传下载（未实现）

分片上传，断点续传


