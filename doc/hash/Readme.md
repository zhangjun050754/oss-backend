# 数据校验和去重

> 每个对象采用 `SHA-256` 算法计算 `hash` 值的  `Base64` 编码 作为对象全局唯一标示

- 上传文件时，服务器计算文件散列值，查库对比

## 注意问题

- 一个对象可能被多次引用，用户进行删除对象时，如果该对象存在其他引用，那么仅能删除用户与对象的关联关系
- 如果对象内容发生变化，且被多次引用，创建并存储一个新的对象
