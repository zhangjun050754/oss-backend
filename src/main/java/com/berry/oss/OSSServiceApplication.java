package com.berry.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xueancao
 */
@SpringBootApplication
public class OSSServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OSSServiceApplication.class, args);
    }
}
