package com.berry.oss.dao.mapper;

import com.berry.oss.dao.entity.RefererInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 防盗链，http referer 白名单设置 Mapper 接口
 * </p>
 *
 * @author HiCooper
 * @since 2019-09-24
 */
public interface RefererInfoMapper extends BaseMapper<RefererInfo> {

}
